# Vuetify Frontend Framework Demo App

Demo for the Vuetify frontend Framework for Metro.

Demo can be seen at:
https://metro-fef-vuetify.herokuapp.com/

To see the interesting part of the source, navigate to [src/App.vue](src/App.vue)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
